<?php

include 'vendor/ganon.php';
function okem_get_tag_with_attr( $attr, $value, $xml, $tag=null ) {
  if( is_null($tag) )
    $tag = '\w+';
  else
    $tag = preg_quote($tag);

  $attr = preg_quote($attr);
  $value = preg_quote($value);

  $tag_regex = "/<(".$tag.")[^>]*$attr\s*=\s*(['\"])$value\\2[^>]*>(.*?)<\/\\1>/";

  preg_match_all($tag_regex,
                 $xml,
                 $matches,
                 PREG_PATTERN_ORDER);

  return $matches[3];
}
function okem_get_tag( $tag, $xml ) {
  $tag = preg_quote($tag);
  preg_match_all('{<'.$tag.'[^>]*>(.*?)</'.$tag.'>.}',
                   $xml,
                   $matches,
                   PREG_PATTERN_ORDER);

  return $matches[1];
}


function okem_get_current_numbers() {
    
     $html = file_get_contents (EUROMILLIONS_ORIGNIAL_PAGE);
     if (!$html){
         return false; 
     }

     $res['numbers'] = okem_get_tag_with_attr('class','euro_num',$html,'p');
     $res['special_numbers'] = okem_get_tag_with_attr('class','euro_num_c',$html,'p');
  
     $date_r = okem_get_tag_with_attr('class','dateTirage mt20 fl',$html,'h3');
     if (!is_array($date_r) && !isset($date_r[0])){
         return false;
     }
     $res['date'] = $date_r[0];

    $code_r =  okem_get_tag_with_attr('class','tirage_my_million',$html,'p');
     if (!is_array($code_r) && !isset($code_r[0])){
         return false;
     }    
    $res['code'] = $code_r[0];
    
    
    return $res;
    
}
 // Parse the google code website into a DOM
  
