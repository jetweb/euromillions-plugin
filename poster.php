<?php


function  okem_post_results($numbers , $specail_numbers ,$code, $date){
    
    $comma_numbers = '';
    $special_ampersand_numbers = '';

    $date = strstr($date," ") ;
    $title = 'Résultats EuroMillions My Million du ' . $date;
    $content = '';
    $content .= '<div class="numbers">';

    $i = 0;
    foreach ($numbers as $number ){
        
        if ($i > 0){
                $comma_numbers = $comma_numbers  . ' ,';
        }
        $comma_numbers = $comma_numbers  . $number ;
        $content .= '<span class="number">';
        $content .= $number;
        $content .= '</span>';
        $i++;    
    }
    
    $i = 0;
    foreach ($specail_numbers as $number ){
        if ($i > 0){
                $special_ampersand_numbers = $special_ampersand_numbers  . ' & ';
        }        $special_ampersand_numbers .= $number;
        $content .= '<span class="special number">';
        $content .= $number;
        $content .= '</span>';
        $i++;
        
    }    
    
    $content .= '</div>';
    $content .= '<div class="code">';
    $content .= 'Code My Million : ';
    $content .= $code;
    $content .= '</div>';

    // Create post object
    $my_post = array(
      'post_title'    => $title,
      'post_content'  => $content,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_category' => array(RESULTS_CATEGORY_ID)
    );

    // Insert the post into the database
    $post_id = wp_insert_post( $my_post );
    
    if ($post_id ){
        
        $metatitle =  $title .' | ' .get_bloginfo() ;
        $metadesc = 'EuroMillions My Million du '.date('d.m.y').'. Résultat tirage : ' . $comma_numbers .' étoiles '.$special_ampersand_numbers.', code My Million '.$code.'. Détail sur Loto EuroMillions';
        
        update_post_meta($post_id, '_yoast_wpseo_title', $metatitle); 
        update_post_meta($post_id, '_yoast_wpseo_metadesc', $metadesc);

    }
    
}