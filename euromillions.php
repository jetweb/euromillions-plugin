<?php

/**
 * Plugin Name: euromillions
 * Description: Auto Update Euromillion Results
 * Version: 1.0.1
 * Author: Oren Kolker
 */




/**
 *  Defintitions
 */



/**
 * Includes
 */

require_once  'timing.php';
require_once  'parser.php';
require_once  'poster.php';


/*
 * Add Stylesheet
 */


add_action( 'wp_enqueue_scripts', 'okem_ouput_css' );
function okem_ouput_css(){
    	wp_enqueue_style( 'euromillions',  plugins_url('style/euromillions.css', __FILE__));

}
define('RESULTS_CATEGORY_ID', 3);
define('EUROMILLIONS_ORIGNIAL_PAGE', 'https://www.fdj.fr/jeux/jeux-de-tirage/euromillions/resultats');
/**
 * Main 
 */

function okem_main() {
    $results = okem_get_current_numbers();
    if (!$results){
        return;
    }
    
    $new_code = wp_strip_all_tags($results['code']);
    $last_code = get_option( 'euromillions_last_code', '0' );
    if ( $last_code != $new_code){
        okem_post_results($results['numbers'] , $results['special_numbers'] ,$new_code, $results['date']);
        update_option('euromillions_last_code',$new_code);
    }
    
    
}


